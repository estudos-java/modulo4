import java.util.Set;

public class Carro {

    public static final String VERMELHO = "Vermelha"; 
    public static final String PRETO    = "Preta"   ; 

    public String VERDE = "verde";
    public Integer quantidadePneus;
    public Integer quantidadeParafusosPneus;
    public Integer numeroDePortas;
    public String cor;
    public String numeroChassi;
    public String anoFabricacao;
    public String combustivel;

    public Carro(Integer quantidadePneus, Integer quantidadeParafusosPneus, Integer numeroDePortas, String cor, String numeroChassi, String anoFabricacao, String combustivel){
        setQuantidadePneus(quantidadePneus);
        setQuantidadeParafusosPneus(quantidadeParafusosPneus);
        setNumeroDePortas(numeroDePortas);
        setCor(cor);
        setNumeroChassi(numeroChassi);
        setAnoFabricacao(anoFabricacao);
        setCombustivel(combustivel);
    }

    public void imprimeValores(){
        System.out.println("\nCARACTERÍSTICAS VEICULARES\n");
        System.out.println("Quantidade de Pneus    : " + getQuantidadePneus());
        System.out.println("Quantidade de Parafusos: " + getQuantidadeParafusosPneus());
        System.out.println("Número de Portas       : " + getNumeroDePortas());
        System.out.println("Cor                    : " + getCor());
        System.out.println("Número do Chassi       : " + getNumeroChassi());
        System.out.println("Ano de Fabricação      : " + getAnoFabricacao());
        System.out.println("Tipo de Combustível    : " + getCombustivel());
    }

    public Integer getQuantidadePneus(){
        return quantidadePneus + 4;
    }

    public void setQuantidadePneus(Integer quantidadePneus){
        this.quantidadePneus = quantidadePneus;
    }

    public Integer getQuantidadeParafusosPneus() {
        return quantidadeParafusosPneus + 4*4;
    }

    public void setQuantidadeParafusosPneus(Integer quantidadeParafusosPneus) {
        this.quantidadeParafusosPneus = quantidadeParafusosPneus;
    }

    public Integer getNumeroDePortas() {
        return numeroDePortas + 4;
    }

    public void setNumeroDePortas(Integer numeroDePortas) {
        this.numeroDePortas = numeroDePortas;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor){
        this.cor = cor;
    }

    public String getNumeroChassi() {
        return numeroChassi + "123ABC456DEF789";
    }

    public void setNumeroChassi(String numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public String getAnoFabricacao() {
        return anoFabricacao + "01/01/2021";
    }

    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public String getCombustivel() {
        return combustivel + "Etanol/Gasolina/GNV";
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }
}